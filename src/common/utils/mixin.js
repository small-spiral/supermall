import { debounce } from "common/utils/debounce.js";
export const itemListenerMixin = {
    data() {
        return {
            imgItemListener: null,
            refresh: null
        }
    },
  mounted() {
    this.refresh = debounce(this.$refs.scrollRef.refresh, 500);
    this.imgItemListener = () => {
      // console.log('啊哈哈');
      this.refresh();
    };
    this.$bus.$on("finishLoadingImage", this.imgItemListener);
    console.log('我好帅');
  },
};
