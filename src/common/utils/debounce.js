//防抖函数
export function debounce(func, delay) {
  let timer = null;
  return function(...args) {
    if(timer) clearTimeout(timer)  //如果有定时器，则先清楚定时器先
    timer = setTimeout(() => {
      func.apply(this, args)
    }, delay)
  }
}