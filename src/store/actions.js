// import Vue from 'vue'
// import Vuex from 'vuex'
import { ADD_COUNTER, ADD_TO_CART } from "./mutations_type"
export default {
    addCart(context, product) {
        return new Promise((resolve) => {
            // state.cartList.push(payLoad)
        let oldProduct = context.state.cartList.find(item => {
            return item.iid === product.iid
        })
        // 如果存在iid相同的，则使这个旧的内部+1
        if(oldProduct) {
            // oldProduct.count += 1
            context.commit(ADD_COUNTER, oldProduct)
            resolve('当前商品数量加1')
        }else {
            // 如果不相同就直接给新添加的数组的count加1， 然后推入cartList数组当中
           product.count = 1;
            context.commit(ADD_TO_CART, product)
            resolve('添加新的商品')
        }
        })
    }
}