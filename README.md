# spuermarket

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).




项目阶段
1.使用git进行项目的托管
2.下载相关的css文件进行初始化
3.重命名一些路径
